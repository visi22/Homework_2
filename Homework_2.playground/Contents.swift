//: Вывод количества дней в каждом месяце с именами месяцев

let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let nameOfMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

for month in 0..<daysInMonth.count {
    print("In \(nameOfMonth[month]) \(daysInMonth[month]) days.")
}




//: Вывод количества дней в каждом месяце с массивом из тюплов

let arrayOfTuple = [("Jan", 31), ("Feb", 28), ("Mar", 31), ("Apr", 30), ("May", 31), ("Jun", 30), ("Jul", 31), ("Aug", 31), ("Sep", 30), ("Oct", 31), ("Nov", 30), ("Dec", 31)]

for month in 0..<arrayOfTuple.count {
    print("In \(arrayOfTuple[month].0) \(arrayOfTuple[month].1) days.")
}




//: Вывод количества дней в каждом месяце с массивом из тюплов в обратном порядке

for month in 0..<arrayOfTuple.count {
    print("In \(arrayOfTuple[arrayOfTuple.count - month - 1].0) \(arrayOfTuple[arrayOfTuple.count - month - 1].1) days.")
}



//: Количество дней до рандомной даты от начала года

let check = (randomMonth: 2 , randomDay: 10 )
var daysBeforeDate = 0

for days in 0..<check.randomMonth - 1 {
    daysBeforeDate += daysInMonth[days]
}

daysBeforeDate += check.randomDay
print("Дней до даты - \(daysBeforeDate)")
